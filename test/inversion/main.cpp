#include "lapack++/lapack++.hpp"

#include <array>
#include <cmath>
#include <cstdlib>
#include <iostream>

bool test();

int main()
{
    return (test() ? EXIT_SUCCESS : EXIT_FAILURE);
}

//!
//! \fn bool test()
//! \brief Test to invert a 3x3 matrix in double precision.
//! \return true if test passed, false otherwise
//!
bool test()
{
    constexpr auto dim = 3ul;
    std::array<std::array<double, dim>, dim> A = {{{1.0, 2.0, 3.0},
                                                   {0.0, 1.0, 4.0},
                                                   {5.0, 6.0, 0.0}}};
    std::array<std::array<double, dim>, dim> Ainv = {{{-24.0, 18.0, 5.0},
                                                      {20.0, -15.0, -4.0},
                                                      {-5.0, 4.0, 1.0}}};

    std::cout << "Matrix to invert:\n";
    for (auto i = 0ul; i < dim; ++i)
    {
        for (auto j = 0ul; j < dim; ++j)
        {
            std::cout << A[i][j] << (j == dim-1 ? "" : " ");
        }
        std::cout << std::endl;
    }

    bool A_is_correctly_inverted = true;
    try
    {
        lapackpp::getri(A);

        std::cout << "Inverted matrix:\n";
        for (auto i = 0ul; i < dim; ++i)
        {
            for (auto j = 0ul; j < dim; ++j)
            {
                std::cout << A[i][j] << (j == dim-1 ? "" : " ");
            }
            std::cout << std::endl;
        }

        for (auto i = 0ul; i < dim; ++i)
        {
            for (auto j = 0ul; j < dim; ++j)
            {
                if (std::fabs(A[i][j] - Ainv[i][j]) > 1.0e-12)
                {
                    A_is_correctly_inverted = false;
                }
            }
        }
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        A_is_correctly_inverted = false;
    }

    return A_is_correctly_inverted;
}
