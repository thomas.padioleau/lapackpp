#pragma once

#include <array>
#include <iostream>
#include <vector>

namespace lapackpp
{

template <class T>
void getrf(int M, int N, T* A, int lda, int* ipiv, int& info);

template <class T>
void getri(int N, T* A, int lda, const int* ipiv, T* work, int lwork, int& info);

//!
//! @fn void getri(std::array<std::array<T, N>, N>& A)
//! @brief Function to invert a matrix A
//!  Storage is row-major, i.e. @f$A[i][j] = a_{i, j}@f$.
//! @param[in,out] A Matrix to invert which is overwritten by it's inverse
//!
template <class T, std::size_t N>
void getri(std::array<std::array<T, N>, N>& A)
{
    std::array<int, N> ipiv;
    int info = 0;
    getrf(N, N, A[0].data(), N, ipiv.data(), info);
    if (info != 0)
    {
        std::string msg;
        std::string info_str = std::to_string(info);
        if (info < 0)
        {
            msg = "Argument "+info_str+" has an illegal value";
        }
        else
        {
            msg = "A["+info_str+","+info_str+"] is exactly zero.\n"
                  "The factorization has been completed, but the factor A is"
                  " exactly singular, and division by zero will occur if"
                  " it is used to solve a system of equations.";
        }
        throw std::runtime_error(msg);
    }

    // Request optimal work space size using lwork=-1 in getri.
    int lwork;
    {
        T work;
        getri(N, A[0].data(), N, ipiv.data(), &work, -1, info);
        lwork = static_cast<int>(work);
    }

    if (lwork < 1)
    {
        throw std::runtime_error("lwork should be positive");
    }

    std::vector<T> work(lwork);
    getri(N, A[0].data(), N, ipiv.data(), work.data(), lwork, info);
    if (info != 0)
    {
        std::string msg;
        std::string info_str = std::to_string(info);
        if (info < 0)
        {
            msg = "Argument "+info_str+" has an illegal value";
        }
        else
        {
            msg = "A["+info_str+","+info_str+"] is exactly zero.\n"
                  "The matrix is singular"
                  " and its inverse could not be computed.";
        }
        throw std::runtime_error(msg);
    }
}

}  // lapackpp
