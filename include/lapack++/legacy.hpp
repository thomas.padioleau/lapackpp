#pragma once

namespace lapackpp
{

namespace legacy
{

extern "C"
{
    void sgetrf_(const int* M, const int* N,  float *A, const int* lda, int* ipiv, int* info);
    void dgetrf_(const int* M, const int* N, double *A, const int* lda, int* ipiv, int* info);

    void sgetri_(const int* N,  float *A, const int* lda, const int* ipiv,  float* work, const int* lwork, int* info);
    void dgetri_(const int* N, double *A, const int* lda, const int* ipiv, double* work, const int* lwork, int* info);
}

}  // legacy

}  // lapackpp
