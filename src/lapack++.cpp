#include "lapack++/lapack++.hpp"
#include "lapack++/legacy.hpp"

namespace lapackpp
{

template <>
void getrf<float>(int M, int N, float* A, int lda, int* ipiv, int& info)
{
    legacy::sgetrf_(&M, &N, A, &lda, ipiv, &info);
}

template <>
void getrf<double>(int M, int N, double* A, int lda, int* ipiv, int& info)
{
    legacy::dgetrf_(&M, &N, A, &lda, ipiv, &info);
}

template <>
void getri<float>(int N, float* A, int lda, const int* ipiv, float* work, int lwork, int& info)
{
    legacy::sgetri_(&N, A, &lda, ipiv, work, &lwork, &info);
}

template <>
void getri<double>(int N, double* A, int lda, const int* ipiv, double* work, int lwork, int& info)
{
    legacy::dgetri_(&N, A, &lda, ipiv, work, &lwork, &info);
}

}  // lapackpp
